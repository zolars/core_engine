'''
  This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330,


Contributor:	"Anusha Kadambala"<anusha.kadambala@gmail.com>
		"Sonal Chaudhari"<chaudhari.sonal.a@gmail.com>
		"Akshay Puradkar"<akshay.aksci@gmail.com>
		"Ujwala Pawade"<ujwalahpawade@gmail.com>
		"Girish Joshi"<girish946@gmail.com>
		"Sadhana Bagal " <sadhana@dff.org.in>
		
'''


#import the database connector and functions for stored procedure.
import dbconnect
#import the twisted modules for executing rpc calls and also to implement the server
from twisted.web import xmlrpc, server
#reactor from the twisted library starts the server with a published object and listens on a given port.
from twisted.internet import reactor
from sqlalchemy.orm.exc import NoResultFound
#from mercurial.templater import if_
#inherit the class from XMLRPC to make it publishable as an rpc service.
class user(xmlrpc.XMLRPC):
	def getUserRoleByName(self,queryParams,client_id):
		#this module returns the user role for the perticular user by it's name.
		connection = dbconnect.engines[client_id].connect()
		Session = dbconnect.session(bind=connection)
		res = Session.query(dbconnect.Users).filter(dbconnect.Users.username == queryParams[0]).first()
		Session.close()
		connection.connection.close()
		if res != None:
			print res.userrole
			return res.userrole
		else:
			print "no user found"
			return False

	def __init__(self):
		xmlrpc.XMLRPC.__init__(self)
#note that all the functions to be accessed by the client must have the xmlrpc_ prefix.
#the client however will not use the prefix to call the functions. 
			
	def xmlrpc_setUser(self,queryParams,client_id):
		#admin can create the user for manager and operator and manager can create user for operator only.
		#another admin user cannot be created and manager cannot create user for manager.
		#the codes for user_roles are as follows
		#-1 : admin
		# 0 : manager
		# 1 : operator
		if ( ((dbconnect.user_roles[client_id]== -1) and ( queryParams[2] == 0 or queryParams[2] == 1)) or ((dbconnect.user_roles[client_id] == 0) and ( queryParams[2] == 1)) ) :
			dbconnect.execproc("addUser",dbconnect.engines[client_id],[queryParams[0],queryParams[1],queryParams[2],queryParams[3],queryParams[4]])
			return 1
		else:
			return -1

	'''def xmlrpc_setPrivilege(self,queryParams):
		print queryParams
		res = dbconnect.executeProcedure("setPrivilege",False,queryParams)
		return True'''
	def xmlrpc_delUser(self, queryParams,client_id):
		"""
		purpose: deletes a user record from the userss table.
		description:
		This function takes one arguement queryParams which contains one element,
		the name of user to be deleted.
		Returns true if successful or false otherwise.
		"""
		success = dbconnect.execproc("removeuser", dbconnect.engines[client_id], queryParams)
		result = success.fetchone()
		return result['success']

	def xmlrpc_getUser(self,queryParams,client_id):
		connection = dbconnect.engines[client_id].connect()
		Session = dbconnect.session(bind=connection)
		res = Session.query(dbconnect.Users).filter(dbconnect.Users.username == queryParams[0]).filter(dbconnect.Users.userpassword == queryParams[1]).first()
		Session.close()
		connection.connection.close()
		if res != None:
			dbconnect.addUser(client_id,queryParams[0],res.userrole)
			lst=[res.username, res.userrole]
			print lst
			print dbconnect.userlist
   			return lst
		else:
			return False
			
#multi return
	'''def xmlrpc_checkUser(self,queryParams,client_id):
		connection = dbconnect.engines[client_id].connect()
		Session = dbconnect.session(bind=connection)
		res = Session.query(dbconnect.Users).filter(dbconnect.Users.username == queryParams[0]).all()
		print res
		if res == []:
			return False
		else:
			return res'''

	def xmlrpc_changePassword(self,queryParams,client_id):
		#getting the role of the user whose password is to be changed.
		targate_user_role = self.getUserRoleByName([queryParams[0]],client_id)
		#if ((dbconnect.user_roles[client_id] == -1) and (targate_user_role == 0 or targate_user_role == 1 )) means  admin is changing the password for the manager or operator then old password is not required.
		#if ((dbconnect.user_roles[client_id] == 0) and (targate_user_role == 1)) means manager is changing the password of the operator so the old password is not required.
		if ( ((dbconnect.user_roles[client_id] == -1) and (targate_user_role == 0 or targate_user_role == 1 )) or ((dbconnect.user_roles[client_id] == 0) and (targate_user_role == 1)) ) :
			result = dbconnect.execproc("adminChangePassword", dbconnect.engines[client_id], queryParams)
			row = result.fetchone()
			return row[0]
		elif ( (dbconnect.user_roles[client_id] == targate_user_role) and (dbconnect.userlist[client_id] == queryParams[0])) :
			#this condition is true when user is changing his own password, so the old password is required here.
			result = dbconnect.execproc("changePassword", dbconnect.engines[client_id], queryParams)
			row = result.fetchone()
			return row[0]
		else :
			#this condition occurs when the user tries to change the password of the other user having same user role or higher authority.
			return False

	def xmlrpc_isUserUnique(self,queryParams,client_id):
		connection = dbconnect.engines[client_id].connect()
		Session = dbconnect.session(bind=connection)
		res = Session.query(dbconnect.Users).filter(dbconnect.Users.username == queryParams[0]).all()
		Session.close()
		connection.close()
		if res == []:
			return True
		else:	
			return False

	#this module will be accessed by the controller so this method is required.
	def xmlrpc_getUserRole(self,queryParams,client_id):
		return self.getUserRoleByName(queryParams,client_id)
			

	def xmlrpc_getUserByClientId(self,client_id):
		return dbconnect.getUserByClientId(client_id)

	#admin/user[True/False],password-yes,password-no[True/False]	
	def xmlrpc_getUsers(self,queryParams,client_id):
		connection = dbconnect.engines[client_id].connect()
		Session = dbconnect.session(bind=connection)
		if queryParams[0]==True:
			role=0
		else:
			role=1
		res = Session.query(dbconnect.Users).filter(dbconnect.Users.userrole==role).all()
		users=[]
		for i in res:
			users.append(i.username)
#			user=[]
#			user.append(i.username)
#			user.append(i.userpassword)
#			users.append(user)
		return users
	
	def xmlrpc_getAllUsers(self,client_id):
		if (dbconnect.user_roles[client_id] == -1):
			stmt = "select username, userrole from users where userrole = '0' or userrole ='1';"
			res= dbconnect.engines[client_id].execute(stmt).fetchall()
			if res == []:
				return False
			else:
				res1 = []
				for l in res: 
					res1.append(l[0])
				return res1
		elif (dbconnect.user_roles[client_id] == 0):
			stmt = "select username, userrole from users where userrole='1';"
			res = dbconnect.engines[client_id].execute(stmt).fetchall()
			if res == []:
				return False
			else:
				res1 = []
				for l in res: 
					res1.append(l[0])
				return res1
		else:
			return False
		
	def xmlrpc_getQuestion(self,queryParams,client_id):
		'''
		purpose: Gets the question and answer for forget password
		'''
		res = dbconnect.execproc("getsecurityquestion", dbconnect.engines[client_id],queryParams)
		#suc = res.fetchone();
		row = res.fetchone()
		qa = [row["user_question"],row["user_answer"]]
		return qa
	
	def xmlrpc_forgotPassword(self,queryparams,client_id):
		res = dbconnect.execproc("forgotPassword", dbconnect.engines[client_id],queryparams)
		row = res.fetchone()
		qa = row["success"]
		return qa
	
		
			
				
			